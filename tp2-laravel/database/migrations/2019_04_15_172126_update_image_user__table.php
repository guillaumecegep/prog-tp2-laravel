<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateImageUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // établissement des clés étrangères   
        Schema::disableForeignKeyConstraints();
        Schema::table('image_user', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');;
            $table->foreign('image_id')->references('id')->on('images')->onDelete('cascade');;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('image_user', function (Blueprint $table) {
            $table->dropForeign('image_user_user_id_foreign');
            $table->dropForeign('image_user_image_id_foreign');
        });
    }
}
