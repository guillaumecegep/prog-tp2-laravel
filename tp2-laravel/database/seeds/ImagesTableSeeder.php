<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class ImagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('images')->insert([
            'title' => 'un beau paysage',
            'name' => 'img01.jpg',
            'user_id' => 2,
            'location_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')          
        ]);
        DB::table('images')->insert([
            'title' => 'un beau paysage',
            'name' => 'img02.jpg',
            'user_id' => 2,
            'location_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')               
        ]);
        DB::table('images')->insert([
            'title' => 'un beau paysage',
            'name' => 'img03.jpg',
            'user_id' => 2,
            'location_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')                
        ]); 
        DB::table('images')->insert([
            'title' => 'un beau paysage',
            'name' => 'img04.jpg',
            'user_id' => 2,
            'location_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')                
        ]);
        DB::table('images')->insert([
            'title' => 'un beau paysage',
            'name' => 'img05.jpg',
            'user_id' => 2,
            'location_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')               
        ]); 
        DB::table('images')->insert([
            'title' => 'un beau paysage',
            'name' => 'img06.jpg',
            'user_id' => 2,
            'location_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')               
        ]); 
        DB::table('images')->insert([
            'title' => 'un beau paysage',
            'name' => 'img07.jpg',
            'user_id' => 2,
            'location_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')                
        ]);   
        DB::table('images')->insert([
            'title' => 'un beau paysage',
            'name' => 'img08.jpg',
            'user_id' => 2,
            'location_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')              
        ]);  
        DB::table('images')->insert([
            'title' => 'un beau paysage',
            'name' => 'img09.jpg',
            'user_id' => 2,
            'location_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')          
        ]);
        DB::table('images')->insert([
            'title' => 'un beau paysage',
            'name' => 'img10.jpg',
            'user_id' => 2,
            'location_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')               
        ]);
        DB::table('images')->insert([
            'title' => 'un beau paysage',
            'name' => 'img11.jpg',
            'user_id' => 2,
            'location_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')                
        ]); 
        DB::table('images')->insert([
            'title' => 'un beau paysage',
            'name' => 'img12.jpg',
            'user_id' => 2,
            'location_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')                
        ]);
        DB::table('images')->insert([
            'title' => 'un beau paysage',
            'name' => 'img13.jpg',
            'user_id' => 2,
            'location_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')               
        ]); 
        DB::table('images')->insert([
            'title' => 'un beau paysage',
            'name' => 'img14.jpg',
            'user_id' => 2,
            'location_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')               
        ]); 
        DB::table('images')->insert([
            'title' => 'un beau paysage',
            'name' => 'img15.jpg',
            'user_id' => 2,
            'location_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')                
        ]);   
        DB::table('images')->insert([
            'title' => 'un beau paysage',
            'name' => 'img16.jpg',
            'user_id' => 2,
            'location_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')              
        ]);
        DB::table('images')->insert([
            'title' => 'un beau paysage',
            'name' => 'img17.jpg',
            'user_id' => 2,
            'location_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')          
        ]);
        DB::table('images')->insert([
            'title' => 'un beau paysage',
            'name' => 'img18.jpg',
            'user_id' => 2,
            'location_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')               
        ]);
        DB::table('images')->insert([
            'title' => 'un beau paysage',
            'name' => 'img19.jpg',
            'user_id' => 2,
            'location_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')                
        ]); 
        DB::table('images')->insert([
            'title' => 'un beau paysage',
            'name' => 'img20.jpg',
            'user_id' => 2,
            'location_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')                
        ]);
        DB::table('images')->insert([
            'title' => 'un beau paysage',
            'name' => 'img21.jpg',
            'user_id' => 3,
            'location_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')               
        ]); 
        DB::table('images')->insert([
            'title' => 'un beau paysage',
            'name' => 'img22.jpg',
            'user_id' => 4,
            'location_id' => 6,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')               
        ]); 
        DB::table('images')->insert([
            'title' => 'un beau paysage',
            'name' => 'img23.jpg',
            'user_id' => 2,
            'location_id' => 7,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')                
        ]);   
        DB::table('images')->insert([
            'title' => 'un beau paysage',
            'name' => 'img24.jpg',
            'user_id' => 2,
            'location_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')              
        ]);  
        DB::table('images')->insert([
            'title' => 'un beau paysage',
            'name' => 'img25.jpg',
            'user_id' => 5,
            'location_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')          
        ]);
        DB::table('images')->insert([
            'title' => 'un beau paysage',
            'name' => 'img26.jpg',
            'user_id' => 6,
            'location_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')               
        ]);
        DB::table('images')->insert([
            'title' => 'un beau paysage',
            'name' => 'img27.jpg',
            'user_id' => 2,
            'location_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')                
        ]); 
        DB::table('images')->insert([
            'title' => 'un beau paysage',
            'name' => 'img28.jpg',
            'user_id' => 2,
            'location_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')                
        ]);
        DB::table('images')->insert([
            'title' => 'un beau paysage',
            'name' => 'img29.jpg',
            'user_id' => 2,
            'location_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')               
        ]); 
        DB::table('images')->insert([
            'title' => 'un beau paysage',
            'name' => 'img30.jpg',
            'user_id' => 3,
            'location_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')               
        ]); 
        DB::table('images')->insert([
            'title' => 'un beau paysage',
            'name' => 'img31.jpg',
            'user_id' => 4,
            'location_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')                
        ]);   
        DB::table('images')->insert([
            'title' => 'un beau paysage',
            'name' => 'img32.jpg',
            'user_id' => 6,
            'location_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')              
        ]);
        DB::table('images')->insert([
            'title' => 'un beau paysage',
            'name' => 'img33.jpg',
            'user_id' => 6,
            'location_id' => 7,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')              
        ]); 
        DB::table('images')->insert([
            'title' => 'un beau paysage',
            'name' => 'img34.jpg',
            'user_id' => 2,
            'location_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')                
        ]); 
        DB::table('images')->insert([
            'title' => 'un beau paysage',
            'name' => 'img35.jpg',
            'user_id' => 2,
            'location_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')                
        ]);
        DB::table('images')->insert([
            'title' => 'un beau paysage',
            'name' => 'img36.jpg',
            'user_id' => 2,
            'location_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')               
        ]); 
        DB::table('images')->insert([
            'title' => 'un beau paysage',
            'name' => 'img37.jpg',
            'user_id' => 3,
            'location_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')               
        ]); 
        DB::table('images')->insert([
            'title' => 'un beau paysage',
            'name' => 'img38.jpg',
            'user_id' => 4,
            'location_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')                
        ]);   
        DB::table('images')->insert([
            'title' => 'un beau paysage',
            'name' => 'img39.jpg',
            'user_id' => 6,
            'location_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')              
        ]);
        DB::table('images')->insert([
            'title' => 'un beau paysage',
            'name' => 'img40.jpg',
            'user_id' => 3,
            'location_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')               
        ]); 
        DB::table('images')->insert([
            'title' => 'un beau paysage',
            'name' => 'img41.jpg',
            'user_id' => 4,
            'location_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')                
        ]);   
        DB::table('images')->insert([
            'title' => 'un beau paysage',
            'name' => 'img42.jpg',
            'user_id' => 6,
            'location_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')              
        ]);
        DB::table('images')->insert([
            'title' => 'un beau paysage',
            'name' => 'img43.jpg',
            'user_id' => 6,
            'location_id' => 7,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')              
        ]); 
        DB::table('images')->insert([
            'title' => 'un beau paysage',
            'name' => 'img44.jpg',
            'user_id' => 2,
            'location_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')                
        ]); 
        DB::table('images')->insert([
            'title' => 'un beau paysage',
            'name' => 'img45.jpg',
            'user_id' => 2,
            'location_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')                
        ]);
        DB::table('images')->insert([
            'title' => 'un beau paysage',
            'name' => 'img46.jpg',
            'user_id' => 2,
            'location_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')               
        ]); 
        DB::table('images')->insert([
            'title' => 'un beau paysage',
            'name' => 'img47.jpg',
            'user_id' => 3,
            'location_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')               
        ]); 
        DB::table('images')->insert([
            'title' => 'un beau paysage',
            'name' => 'img48.jpg',
            'user_id' => 4,
            'location_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')                
        ]);   
        DB::table('images')->insert([
            'title' => 'un beau paysage',
            'name' => 'img49.jpg',
            'user_id' => 6,
            'location_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')              
        ]); 
        DB::table('images')->insert([
            'title' => 'un beau paysage',
            'name' => 'img50.jpg',
            'user_id' => 3,
            'location_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')               
        ]); 
        DB::table('images')->insert([
            'title' => 'un beau paysage',
            'name' => 'img51.jpg',
            'user_id' => 4,
            'location_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')                
        ]);   
        DB::table('images')->insert([
            'title' => 'un beau paysage',
            'name' => 'img52.jpg',
            'user_id' => 6,
            'location_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')              
        ]);
        DB::table('images')->insert([
            'title' => 'un beau paysage',
            'name' => 'img53.jpg',
            'user_id' => 6,
            'location_id' => 7,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')              
        ]); 
        DB::table('images')->insert([
            'title' => 'un beau paysage',
            'name' => 'img54.jpg',
            'user_id' => 2,
            'location_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')                
        ]); 
        DB::table('images')->insert([
            'title' => 'un beau paysage',
            'name' => 'img55.jpg',
            'user_id' => 2,
            'location_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')                
        ]);
        DB::table('images')->insert([
            'title' => 'un beau paysage',
            'name' => 'img56.jpg',
            'user_id' => 2,
            'location_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')               
        ]); 
        DB::table('images')->insert([
            'title' => 'un beau paysage',
            'name' => 'img57.jpg',
            'user_id' => 3,
            'location_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')               
        ]); 
        DB::table('images')->insert([
            'title' => 'un beau paysage',
            'name' => 'img58.jpg',
            'user_id' => 4,
            'location_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')                
        ]);   
        DB::table('images')->insert([
            'title' => 'un beau paysage',
            'name' => 'img59.jpg',
            'user_id' => 6,
            'location_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')              
        ]); 
        DB::table('images')->insert([
            'title' => 'un beau paysage',
            'name' => 'img60.jpg',
            'user_id' => 3,
            'location_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')               
        ]); 
        DB::table('images')->insert([
            'title' => 'un beau paysage',
            'name' => 'img61.jpg',
            'user_id' => 4,
            'location_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')                
        ]);   
        DB::table('images')->insert([
            'title' => 'un beau paysage',
            'name' => 'img62.jpg',
            'user_id' => 6,
            'location_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')              
        ]);
                                    
    }
}
