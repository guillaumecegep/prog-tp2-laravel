<?php

use Illuminate\Database\Seeder;

class LocationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('locations')->insert([
            'name' => 'Québec'            
        ]);
        DB::table('locations')->insert([
            'name' => 'Montréal'            
        ]);
        DB::table('locations')->insert([
            'name' => 'Mont-Joli'            
        ]); 
        DB::table('locations')->insert([
            'name' => 'Price'            
        ]);
        DB::table('locations')->insert([
            'name' => 'Saint-Angèle-de-Mérici'            
        ]); 
        DB::table('locations')->insert([
            'name' => 'Saint-Octave'            
        ]); 
        DB::table('locations')->insert([
            'name' => 'Sainte-Flavie'            
        ]);   
        DB::table('locations')->insert([
            'name' => 'Point-Au-Père'            
        ]);                     
    }
}
