<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class ImageUserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('image_user')->insert([
            'report' => 1,
            'user_id' => 2,
            'image_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')          
        ]);
        DB::table('image_user')->insert([
            'report' => 1,
            'user_id' => 1,
            'image_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')          
        ]);
        DB::table('image_user')->insert([
            'report' => 1,
            'user_id' => 2,
            'image_id' => 6,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')          
        ]);
        DB::table('image_user')->insert([
            'report' => 1,
            'user_id' => 1,
            'image_id' => 6,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')          
        ]);
        DB::table('image_user')->insert([
            'report' => 1,
            'user_id' => 2,
            'image_id' => 8,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')          
        ]);
        DB::table('image_user')->insert([
            'report' => 1,
            'user_id' => 1,
            'image_id' => 8,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')          
        ]);
    }
}
