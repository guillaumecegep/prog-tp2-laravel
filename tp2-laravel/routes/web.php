<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::resource('/', 'PagesController');

Route::put('images/{id}','ImagesController@report');
Route::post('images/refresh/{image}', 'ImagesController@refresh');
Route::resource('images','ImagesController');

Route::resource('locations','LocationsController');
Route::delete('locations/destroy-loc/{id}', 'LocationsController@destroyLoc')->name('LocDestroy');
Route::delete('locations/destroy-loc-ajax/{id}', 'LocationsController@destroy')->name('LocDestroyAjax');

Auth::routes(['verify' => true]);

Route::get('/search', 'SearchController@index')->name('Search'); 
Route::get('/search/{request}', 'SearchController@index')->name('SearchAjax'); 
Route::get('/search-auto', 'SearchController@search')->name('SearchController.search');


Route::resource('user','UsersController');
Route::delete('user/destroy-account/{id}', 'UsersController@destroy_account')->name('UserDestroy');



Route::get ('dashboard', 'AdminsController@index')->name('dashboard');
Route::any ('reported', 'AdminsController@reportedList')->name('reported');
Route::delete('reported/clear', 'AdminsController@destroyAllReports');


// Route::middleware('admin')->group(function () {
//   Route::resource ('admin', 'AdminsController');
//   Route::resource ('user', 'UsersController');
// });


