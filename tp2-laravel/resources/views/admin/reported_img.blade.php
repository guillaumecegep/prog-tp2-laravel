@extends('layouts.app')

@section('content')
<div class="container mt--8 pb-5">
    <div class="row justify-content-center">
        <div class="col-lg-8 col-md-10">
            <div class="card bg-dark shadow border-0">
                <div class="card-header bg-transparent pb-3">
                    <div class="text-center">
                        <h3>
                            Liste des images signalées
                        </h3>
                    </div>
                </div>
                <div class="card-body px-0">
                    <table class="table table-striped table-dark">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Image</th>
                                <th scope="col">#Signalement?</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody id="reported-images">
                            @foreach ($images as $image)
                            <tr id="user-tr-{{$image->user_id}}">
                                <th scope="row">{{ $image->id }}</th>
                                <td><img style="max-width:40px;"src="/storage/thumbs/{{ $image->name }}" alt="image reporté"></td>
                                <td>{{ $image->users->where('pivot.report', 1)->count() }}</td>
                                <td class="d-flex">
                                    @include('inc.btn_edit') 
                                    @include('inc.btn_delete')
                                    @include('inc.btn_report_refresh')</td>
                                
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            @admin
                @include('inc.btn_delete_all')
            @endadmin
        </div>
    </div>
</div>

@endsection