@extends('layouts.app')

@section('content')
<div class="container mt--8 pb-5">
    <div class="row justify-content-center">
        <div class="col-lg-8 col-md-10">
            <div class="card bg-dark shadow border-0">
                <div class="card-header bg-transparent pb-3">
                    <div class="text-center">
                        <h3>
                            Liste des utilisateurs
                        </h3>
                    </div>
                </div>
                <div class="card-body px-0">
                    <table class="table table-striped table-dark">
                        <thead>
                            <tr>
                                <th scope="col">Nom</th>
                                <th scope="col">Courriel</th>
                                <th scope="col">Inscription</th>
                                <th scope="col">Vérification</th>                                
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($users as $user)
                                <tr id="user-{{$user->id}}">
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>{{ $user->created_at }}</td>
                                    <td>@if($user->email_verified_at){{ $user->email_verified_at }}@endif</td>
                                    <td class="d-flex">                                   
                                        @include('inc.user_btn_edit')
                                        
                                        @if($user->role==="user")
                                            @include('inc.user_btn_delete')
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection