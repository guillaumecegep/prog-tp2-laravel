@if (session()->has('wrong'))
    <div class="alert alert-danger text-center animated flash">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <strong>
            {!! session()->get('wrong') !!}
        </strong>
        
    </div>
@endif