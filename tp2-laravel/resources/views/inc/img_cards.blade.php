        <div class="card-columns">
            @foreach ($images as $image)
                <div id="image-{{ $image->id }}">
                    <div class="card bg-dark text-white">
                        <a href="{{ url("/images/{$image->id}") }}">
                            <img class="card-img" src="/storage/thumbs/{{ $image->name }}" alt="Card image">
                        </a>
                        <div style="height:15%;" class="card-img-overlay">
                            {{ $image->location->name }}
                
                            <div class="buttons">
                                @include('inc.btn_report')
                                @include('inc.btn_delete')
                            </div>
                        </div>
                        <div class="user-photo pl-2">
                            <i class="fas fa-user pr-2"></i>{{ $image->user->name }} <br>
                            {{ $image->users->where('pivot.report', 1)->count() }}
                
                        </div>
                    </div>
                </div>
            @endforeach
 

        </div>
        <div class="col-12 d-flex justify-content-center flex-wrap">
        {{ $images->links() }}
        </div>