@admin
<!-- url pas encore faite!!! -->
<form action="{{ url('reported/clear') }}" method="POST" class="form-delete-reports">  
    {{ csrf_field() }}
    {{ method_field('DELETE') }}

    <button type="submit" class="btn btn-danger m-1">
        Tout supprimer
    <i class="far fa-trash-alt"></i>
    </button>
</form>
@endadmin