@admin
<form action="{{ url('images/refresh/'.$image->id) }}" method="post" class="form-report-refresh">  
    {{ csrf_field() }}

    <button type="submit" class="btn btn-success ml-1">
    <i class="fas fa-redo"></i>
    </button>
</form>
@endadmin