@can('destroy', $image)
{{-- @admin --}}
<form action="{{ url('images/'.$image->id) }}" method="POST" class="form-delete">
    {{ csrf_field() }}
    {{ method_field('DELETE') }}

    <button type="submit" class="btn btn-danger ml-1">
    <i class="far fa-trash-alt"></i>
    </button>
</form>
{{-- @endadmin --}}
@endcan


