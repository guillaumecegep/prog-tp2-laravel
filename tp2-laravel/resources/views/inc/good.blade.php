@if (session()->has('good'))
    <div class="alert alert-success text-center animated flash">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <strong>
            {!! session()->get('good') !!}
        </strong>
    </div>
@endif