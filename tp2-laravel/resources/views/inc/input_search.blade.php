
<div class="d-flex flex-column absolute">
    <form id="form-search" method="GET" action="/search">
    @csrf
    {{ method_field('GET') }}
    <div class="input-group">
        <input class="form-control" type="search" name="search" id="search" placeholder="Rechercher un lieu" aria-label="Search" autocomplete="off">
        <span class="input-group-prepend">
            <button class="btn btn-outline-success" type="submit">Chercher</button>
        </span>
    </div>
    </form>
    <div id="locations_list">

    </div>
</div>
    