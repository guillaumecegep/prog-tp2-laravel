@auth
    <form action="{{ url('images/'.$image->id) }}" method="PUT" class="form-report">
        {{ csrf_field() }}  
        {{ method_field('PUT') }}
        <button type="submit" class="btn btn-warning">
        <i class="far fa-flag"></i>
        </button>
    </form>
@endauth