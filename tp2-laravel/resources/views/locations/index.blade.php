@extends('layouts.app')

@section('content')
<div class="container mt--8 pb-5">
    <div class="row justify-content-center">
        <div class="col-lg-8 col-md-10">
            <div class="card bg-dark shadow border-0">
                <div class="card-header bg-transparent pb-3">
                    <div class="text-center">
                        <h3>
                            Liste des lieux
                        </h3>
                    </div>
                </div>
                <div class="card-body px-0">
                    <table class="table table-striped table-dark">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Nom</th>
                                <th scope="col">Action</th>                                
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($locations as $location)
                            <tr id="location-{{$location->id}}">
                                <th scope="row">{{ $location->id }}</th>
                                <td>{{ $location->name }}</td>
                                <td class="d-flex">@include('inc.loc_btn_edit')
                                    @include('inc.loc_btn_delete')</td>                               
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection