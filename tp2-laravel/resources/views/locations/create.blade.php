@extends('layouts.app')

@section('content')
<div class="container mt--8 pb-5">
    <div class="row justify-content-center">
        <div class="col-lg-6 col-md-8">
            <div class="card bg-dark shadow border-0">
                <div class="card-header bg-transparent pb-3">
                    <div class="text-center">
                        <h3>
                            Ajouter un lieu
                        </h3>
                    </div>
                </div>
                <div class="card-body px-lg-5 py-lg-5">
                    <form action="{{ url("locations") }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        @if ($errors->any())
                        <div class="alert alert-danger" role="alert">
                            Vérifiez les informations
                        </div>
                        @endif
                        <div class="form-group  {{ $errors->has('name') ? 'is-invalid' : '' }}">
                            <div class="input-group input-group-alternative mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fas fa-map-pin"></i></span>
                                </div>
                                <input type="text" name="name" id="task-name" class="form-control"
                                    value="{{ old('name') }}">

                            </div>
                            @if($errors->has('name'))
                            <span class="invalid-feedback" style="display: block;" role="alert">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="text-center">
                            <button type="submit" class="btn btn-primary mt-4">
                                Ajouter
                            </button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
