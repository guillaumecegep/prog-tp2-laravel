@extends('layouts.app')

@section('content')
<div class="container mt--8 pb-5">
        <div class="row justify-content-center">
            <div class="col-lg-6 col-md-8">
                <div class="card bg-dark shadow border-0">
                    <div class="card-header bg-transparent pb-3">                        
                        <div class="text-center">
                            <h3>
                                Modifier mon compte
                            </h3>                          
                        </div>
                    </div>
                    <div class="card-body px-lg-5 py-lg-5">
                        @if ($users)
                            <form role="form" method="POST" action="{{ url('user/'.$users->id) }}">
                                @csrf
                                {{ method_field('PUT') }}
                                @if ($errors->any())
                                    <div class="alert alert-danger" role="alert">
                                        Vérifiez les informations
                                    </div>
                                @endif
                                <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                                    <div class="input-group input-group-alternative mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="far fa-id-badge"></i></span>
                                        </div>
                                        <input class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" placeholder="{{ $users->name }}" type="text" name="name" value="{{ $users->name }}" required autofocus>
                                    </div>
                                    @if ($errors->has('name'))
                                        <span class="invalid-feedback" style="display: block;" role="alert">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group{{ $errors->has('email') ? ' has-danger' : '' }}">
                                    <div class="input-group input-group-alternative mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="far fa-envelope"></i></span>
                                        </div>
                                        <input class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="{{ $users->email }}" type="email" name="email" value="{{ $users->email }}" required>
                                    </div>
                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" style="display: block;" role="alert">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group{{ $errors->has('password') ? ' has-danger' : '' }}"> 
                                    <div class="input-group input-group-alternative">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fas fa-key"></i></span>
                                        </div>
                                        <input class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="{{ __('Mot de passe') }}" type="password" name="password" required>
                                    </div>
                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback" style="display: block;" role="alert">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <div class="input-group input-group-alternative">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fas fa-key"></i></span>
                                        </div>
                                        <input class="form-control" placeholder="{{ __('Confirmation du mot de passe') }}" type="password" name="password_confirmation" required>
                                    </div>
                                </div>
                            
                            
                                <div class="text-center">
                                    <button type="submit" class="btn btn-primary mt-4">{{ __('Enregister les modifications') }}</button>
                                  
                                    
                                    
                                 
                                </div>
                            </form>
                            <form action="{{ url('user/destroy-account/'.$users->id) }}" method="POST" class="text-center form-delete-user-form">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                            
                                <button type="submit" class="btn btn-danger mt-4">{{ __('Supprimer mon compte') }}</button>
                            </form>
                                    
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
