@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
            @if (session('status'))
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">                   
                    <div class="alert alert-warning" role="alert">                       
                        {{ session('status') }}                        
                    </div> 
                </div>           
            </div>
        </div>
        @endif  
        <div class="card-columns">
            @foreach ($images as $image)
                <div id="image-{{ $image->id }}">
                    <div class="card bg-dark text-white">
                        <a href="{{ url("/images/{$image->id}") }}">
                            <img class="card-img" src="/storage/thumbs/{{ $image->name }}" alt="Card image">
                        </a>
                        <div style="height:15%;" class="card-img-overlay">
                            {{ $image->location->name }}
                
                            <div class="buttons">
                                @include('inc.btn_report')
                                @include('inc.btn_delete')
                            </div>
                        </div>
                        <div class="user-photo py-1 pl-2">
                            <i class="fas fa-user pr-2"></i>{{ $image->user->name }}                
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>
@endsection
