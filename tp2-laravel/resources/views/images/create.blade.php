@extends('layouts.app')

@section('content')
<div class="container mt--8 pb-5">
    <div class="row justify-content-center">
        <div class="col-lg-6 col-md-8">
            <div class="card bg-dark shadow border-0">
                <div class="card-header bg-transparent pb-3">
                    <div class="text-center">
                        <h3>
                            Ajouter une photo
                        </h3>
                    </div>
                </div>
                <div class="card-body px-lg-5 py-lg-5">
                    <form action="{{ url("images") }}" method="POST"
                        enctype="multipart/form-data">
                        @csrf
                        @if ($errors->any())
                        <div class="alert alert-danger" role="alert">
                            Vérifiez les informations
                        </div>
                        @endif
                        <div class="form-group {{ $errors->has('title') ? 'is-invalid' : '' }}">
                            <div class="input-group input-group-alternative mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fas fa-camera-retro"></i></span>
                                </div>                            
                                <input type="text" name="title" id="task-name"
                                class="form-control " placeholder="{{ __('Titre') }}"
                                value="{{ old('title') }}" required autofocus>
                            </div>
                            @if($errors->has('title'))
                            <span class="invalid-feedback" style="display: block;" role="alert">
                                <strong>{{ $errors->first('title') }}</strong>
                            </span>
                            @endif                            
                        </div>                       

                        <div class="form-group">
                            <div class="input-group input-group-alternative mb-3">
                                <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fas fa-map-pin"></i></span>
                                </div> 
                                <select class="form-control" name="location" id="location">                                    
                                    @foreach($locations as $id => $name)
                                    <option value="{{ $id }}">
                                        {{ $name }}
                                    </option>
                                    @endforeach
                                </select>
                            </div>
                            <button type="button" class="mt-1 btn btn-info" id="button-location">Ajouter un
                                lieu</button>
                        </div>
                        <div class="form-group">
                            <div class="input-group input-group-alternative mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fas fa-calendar-alt"></i></span>
                                </div> 
                                <input type="datetime-local" name="date" id="date" class="form-control" value="{{ old('date') }}" autofocus>
                            </div>                            
                        </div>
                        
                         <div id="form-upload" class="form-group{{ $errors->has('image') ? ' is-invalid' : '' }}">
                            <div class="input-group input-group-alternative mb-3">
                                <div class="input-group-prepend image">
                                        <span class="input-group-text py-2"><i class="fas fa-file-image"></i></span>
                                </div> 
                                <label class="custom-file-label ml-5 image" for="image">Choisissez une image</label>
                                <input type="file" id="image" name="image"
                                    class="{{ $errors->has('image') ? ' is-invalid ' : '' }} custom-file-input"
                                    lang="fr" required autofocus>
                            </div>
                                @if ($errors->has('image'))
                                <span class="invalid-feedback" style="display: block;" role="alert">
                                    <strong>{{ $errors->first('image') }}</strong>
                                </span>
                                @endif                            
                        </div>

                        <div class="text-center">
                            <button type="submit" class="btn btn-primary mt-4">
                                Ajouter
                            </button>
                        </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
