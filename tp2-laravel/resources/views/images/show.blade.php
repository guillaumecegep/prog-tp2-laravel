@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        @if ($image->name)
        <div id="image-{{ $image->id }}">
                    <div class="card bg-dark text-white">                        
                            <img class="card-img" src="/storage/images/{{ $image->name }}" alt="Card image">                        
                        <div class="card-img-overlay pt-4">
                            <h2>
                        {{ $image->title }} à {{ $image->location->name }} <br>
                        <small><i class="fas fa-user pr-2"></i>{{ $image->user->name }}</small>
                            </h2>
                
                            <div class="buttons pt-4 pr-4">
                                @include('inc.btn_report')
                                @include('inc.btn_delete')
                            </div>
                        </div>
                        
                    </div>
                </div>
        @endif
        
        <div class="col-md-12">
          <h2>
             
          </h2>
         
    </div>
</div>
@endsection

