$(() => {
    $.ajaxSetup({
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
    })
    // liens des boutons
    $('#button-insert').click(function() {
        window.location.href = "/images/create"
        });
    $('#button-location').click(function() {
        window.location.href = "/locations/create"
        });

    // supprimer une image
    $('.form-delete').submit((e) => {
        e.preventDefault();
        let href = $(e.currentTarget).attr('action')
        if (confirm('Voulez-vous réellement supprimer cette image ?')){
            $.ajax({
                url: href,
                type: 'DELETE'
            })
            .done((data) => {
                document.getElementById("image-" + data.id).remove();
                alert(data.message);
            })
            .fail((data) => {
                alert(data.message);
            })
        } 
    })

    //Supprimer un user
    $('.form-delete-user').submit((e) => {
        e.preventDefault();
        let href = $(e.currentTarget).attr('action')
        if (confirm('Voulez-vous réellement supprimer cet utilisateur ?')){
            $.ajax({
                url: href,
                type: 'DELETE'
            })
            .done((data) => {
                document.getElementById("user-" + data.id).remove();
                // alert(data.message);
            })
            .fail((data) => {
                alert(data.message);
            })
        } 
    })

    //Supprimer son compte
    $('.form-delete-user-form').submit((e) => {
        let href = $(e.currentTarget).attr('action')
        if (confirm('Voulez-vous réellement supprimer votre compte ?')){
            window.location.href = href;   
        } 
    })

    //Supprimer un lieu
    $('.form-delete-loc').submit((e) => {
        e.preventDefault();
        let href = $(e.currentTarget).attr('action')
        if (confirm('Voulez-vous réellement supprimer ce lieu ?')){
            $.ajax({
                url: href,
                type: 'DELETE'
            })
            .done((data) => {
                document.getElementById("location-" + data.id).remove();
                alert(data.message);
            })
            .fail((data) => {
                alert(data.message);
            })
        } 
    })

    //Supprimer toutes les images signalée
    $('.form-delete-reports').submit((e) => {
        e.preventDefault();
        let href = $(e.currentTarget).attr('action')
        if (confirm('Voulez-vous réellement supprimer toutes les images signalées?')){
            $.ajax({
                url: href,
                type: 'DELETE'
            })
            .done((data) => {
                document.getElementById("reported-images").remove();
                alert(data.message);
            })
            .fail((data) => {
                alert(data.message);
            })
        } 
    })

    //Réinitialiser une image signalée
    $('.form-report-refresh').submit((e) => {
        e.preventDefault();
        let href = $(e.currentTarget).attr('action')
        if (confirm('Voulez-vous réellement réinitialiser les signalements de cette image?')){
            $.ajax({
                url: href,
                type: 'POST'
            })
            .done((data) => {
                document.getElementById("user-tr-" + data.id).remove();
                alert(data.message);
            })
            .fail((data) => {
                alert(data.message);
            })
        } 
    })

    // signaler une image
   
    $('.form-report').submit((e) => {
        e.preventDefault();
        let href = $(e.currentTarget).attr('action')
        if (confirm('Voulez-vous réellement signaler cette image ?')){
            $.ajax({
                url: href,
                type: 'PUT'
            })
            .done((data) => {
                alert(data.message)
            })
            .fail(() => {
                alert('L\'image n\'a pas pu être signalé!');
            })
        }
    
    })

    // autocomplete
    $('#search').on('keyup',function() {
        var query = $(this).val();
        //console.log('query = ' + query)
        $.ajax({
            url:"search-auto",           
            type:"GET",
            data:{'search':query},
            success:function (data) {
                //console.log('data' +data)
                $('#locations_list').html(data);
            }
        })
    });
    
    //recherche et nettoyage de la fenêtre après le click
    $('#locations_list').on('click', 'li', function(){
        var value = $(this).text();
        $('#search').val(value);
        $('#form-search').submit();
    });
})




