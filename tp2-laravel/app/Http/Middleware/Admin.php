<?php

namespace App\Http\Middleware;

use Closure;
use Session;
class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // on place le user dans la variable user
        $user = $request->user();
 
        //admin?
        if ($user && $user->admin) {
            //true->ok
            return $next($request);
        }        
    

        //false->retour à l'accueil
        return redirect()->route('login');

    }
}
