<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Validator;

use Intervention\Image\Facades\Image as InterventionImage;
use Illuminate\Support\Facades\Storage;

use App\Location;
use App\Image;
use App\User;
use Carbon\Carbon;
use Session;

class ImagesController extends Controller
{
    public function __construct(){
        $this->middleware('auth', ['except' => ['index','show']]);
        $this->middleware('auth', ['only' => ['create', 'update', 'edit', 'store']]);
        $this->middleware('ajax')->only('destroy','report', 'reportRefresh');
    }
    
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $images = Image::orderBy('created_at', 'desc')->paginate(8);
        return view('images.index', compact('images'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //on liste les lieux pour les renvoyer
        $locations = Location::pluck('name','id');
        return view('images.create', compact('locations'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //VALIDER LA REQUÊTE

        $request->validate ([
            'image' => 'required|image|max:30000',
            'name' => 'nullable|string|max:255',
            'title' => 'required|string|max:255',
            'location'=>'required'
        ]);

        // Enregistre l'image originale dans le dossier '/storage/app/public/images'
        $path = basename ($request->image->store('images', 'public'));

        // Enregistre l'image réduite dans le dossier '/storage/app/public/thumbs'
        $image = InterventionImage::make($request->image)->widen(300)->encode();
        Storage::put('public/thumbs/' . $path, $image);      
       
        //ENREGISTREMENT DANS LA BD
        $image = new Image;
        $image->name = $path;
        $image->title = $request->title;
        $image->location_id = $request->location;
        $date =$request->date;
        $image->created_at = Carbon::parse($date); 

        $request->user()->images()->save($image);

        return back()->with('good', __("L'image a bien été enregistrée"));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Image $image)
    {
        return view('images.show', compact('image'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Image $image)
    {
        //on liste les lieux pour les renvoyer
        $locations = Location::pluck('name','id');

        return view('images.edit', compact('image','locations'));
    }
    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Image $image)
    {
        $image->update(request()->all());
        return redirect('/images');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Image $image)
    {
        $this->authorize('destroy', [$user, $image]);

        if ($image->delete()){
            return response()->json(['id' => $image->id, 'message' => 'Image supprimée'],200);
        } else {
            return response()->json(['message' => 'image non trouvée!'], 404);
        }
    }

    public function report(Image $image, Request $request, $id)
    {   
        // recercher le id de l'image
        $images = Image::find($id); 
        //on récupère le id de l'utilisateur
        $user = $request->user()->id;

        $images->users()->syncWithoutDetaching([$user => ['report'=>1]]); 

        return response()->json(['message' => 'L\'image a bien été singalée!'], 200);          
    }
    
    public function refresh(Image $image)
    {   
        // recercher le id de l'image
        $images = Image::find($image->id); 
        $images->users()->sync([]); 

        return response()->json(['message' => 'L\'image a bien été réinitialisée!', 'id' => $image->user_id], 200);          
    }  
  
}
