<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// use Illuminate\Support\Facades\Validator;

use App\User;
use App\Image;
use Session;

class UsersController extends Controller
{
    public function __construct()
    {

        $this->middleware('auth', ['only' => ['update', 'edit', 'store', 'destroy', 'destroy_account']]);
        $this->middleware('ajax')->only('destroy');
    }
    public function index()
    {
        return view('welcome');
    }

    public function edit(User $user, Request $request)
    {
        $this->authorize('edit', $user);
    $users = User::where('id','LIKE','%'.$user->id.'%')->first();
        
        return view('users.edit', compact('users'));
    }

    public function update(Request $request, $id)
    {
        $this->authorize('update', $user);
        $this->validate($request, [
            'name' => ['required', 'string', 'min:3','max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'exists:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
            ]);

        // enregistrement dans la bd
        $user = User::find($id);
        $user->name = $request->input('name');         
        $user->email = $request->input('email');
        $user->password = bcrypt($request->input('password'));
       
        $user->save();
    

        return redirect('/')->with('good', __("Le compte a bien été mis à jour!"));
    }

    public function destroy(User $user)
    {
        //AJAX
        $this->authorize('destroy', $user);

        if ($user->delete()){
            //Requête pour aller chercher les images de l'utilisateur
            $images = Image::where('user_id','LIKE', '%'.$user->id.'%')->get();
            foreach($images as $image){
                $image->delete();
            }
            return response()->json(['id' => $user->id, 'message' => 'utilisateur supprimée'],200);
        } else {
            return response()->json(['message' => 'user non trouvée!'], 404);
        }
    }

    public function destroy_account(User $user, Request $request, $id)
    {
        $user = User::find($id);
        $this->authorize('destroy_account', $user);
         
        if ($user->delete()){
            //Requête pour aller chercher les images de l'utilisateur
            $images = Image::where('user_id','LIKE', '%'.$user->id.'%')->get();
            foreach($images as $image){
                $image->delete();
            }
            return redirect('/')->with('good', __("Le compte a bien été supprimé!"));
        } else {
            return back()->with('wrong', __("Une erreur est servenue!"));
        }
    }
}
