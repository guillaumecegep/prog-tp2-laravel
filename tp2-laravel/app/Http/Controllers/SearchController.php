<?php

namespace App\Http\Controllers;

use App\Location;
use App\Image;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;



class SearchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {


            $q = $request->get('search');


            // SI LE REQUÊTE EST VIDE, ON RENVOIT
            if ($request->get('search') == "") { 
                return redirect('/')->with('wrong', __ ("Aucun terme entré!"));
            }           

            // ON SORT UN TABLEAU DES LIEUX CORRESPONDANTS
            $l = Location::where('name', 'LIKE', '%'.$q.'%')->get(); 
            
            // SI LE TABLEAU EST VIDE, ON RENVOIT 
            if (count($l)===0) {
                return redirect('/')->with('wrong', __ ("Aucun lieux correspondant"));         
            }
                 
            else{
                for ($i = 0; $i < count($l); $i++){
                    
                        $id_loc=$l[$i]->id;
                        $images = Image::where('location_id','LIKE', '%'.$id_loc.'%')->orderBy('created_at','desc')->get(); 
                        
                        $images = $this->getReportedImage($images)->where('approved', 1);

                        //PAGINATION POUR LA RECHERCHE
                        $pageStart = request()->get('page', 1); // récupère le numéro de page dans l'url
                        $perPage = 8; // Défini le nombre d'image par page
                         
                        // Défini le décalage, si on est sur la 1ere page, $offset = 0
                        //si on est sur la 2eme page, $offset = 6
                        $offset = ($pageStart * $perPage) - $perPage; 
                         
                        // Instancie la class Paginator
                        $images = new Paginator(
                            array_slice($images->all(), $offset,  $perPage, true),
                            $images->count(),
                            $perPage,
                            null, 
                            [
                                'path'  => $request->url(),
                                'query' => $request->query(),
                            ]
                        );

                        $images->appends(['search' => $request->post('search')]);
                }
                return view('search.search', compact('images','q'));
            }
            //dump($images);
            
            
            
        }



    public function search(Request $request) 
    { 
        if($request->ajax()) 
        { 
            //Recherche des termes corespondant dans la bd
           
            $query = $request->get('search'); 
            $data = Location::where('name', 'LIKE', $query.'%')->get(); 
            $output = '';
           
            if (count($data)>0){
                $output = '<ul id="searchPrediction" class="list-group" >';
                foreach ($data as $row){
                    $output .= '<li class="list-group-item">'.$row->name.'</li>';
                }
                $output .= '</ul>';
            }
            else {
                $output .= '<li class="list-group-item">'.'Aucun résultat'.'</li>';
            }
            return $output;
        }


    } 

    public function getReportedImage($images)
    {
        $images->transform(function($image) {
            $number = $image->users->where('pivot.report', 1)->count();
            $image->approved = ($number >= 2) ? 0 : 1;
            return $image;
        });
        return $images;
    }
} 

