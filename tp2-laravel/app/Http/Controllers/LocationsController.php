<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Location;
use App\Image;


class LocationsController extends Controller
{
    public function __construct(){
        $this->middleware('auth', ['except' => ['index']]);
        $this->middleware('auth', ['only' => ['create', 'store']]);
        $this->middleware('admin', ['only' => ['update', 'edit', 'store', 'destroy', 'destroyLoc']]);
        $this->middleware('ajax')->only('destroy','report');
    }
   
   /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $locations = Location::orderBy('id', 'asc')->paginate(200);
        return view('locations.index', compact('locations'));
    }
    public function show()
    {
        $locations = Location::orderBy('id', 'asc')->paginate(200);
        return view('locations.index', compact('locations'));
    }
   
    public function create()
    {
        //on liste les lieux
        $locations = Location::pluck('name','id');
        return view('locations.create', compact('locations'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //VALIDER LA REQUÊTE

        $request->validate ([
            'name' => 'required|string|alpha_dash|max:255',
        ]);        
            $q=$request->name;


        //on vérifie que le lieux n'existe pas déjà   
        $dataCheck = Location::where('name', 'LIKE', '%'.$q.'%')->get();             
        
        if(count($dataCheck) == 0)
        {                
            //ENREGISTREMENT DANS LA BD
            
            $location['name'] = $request->name;
            Location::create($location);

            return redirect('images/create')->with('good', __("Le lieu a bien été ajouté"));
        }
        else{
            return back()->with('wrong', __("Le lieu existe déjà"));
        }
    
       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */


    public function edit(Location $location, Request $request)
    {
        $locations = Location::where('id','LIKE','%'.$location->id.'%')->first();
        
        return view('locations.edit', compact('locations'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|string|alpha_dash|max:255',
            ]);

        // enregistrement dans la bd
        $location = Location::find($id);
        $location->name = $request->input('name');       
        
       
        $location->save();
    

        return redirect('locations/index')->with('good', __("Le compte a bien été mis à jour!"));
    }


    public function destroy(Location $location, $id)
    {
        //AJAX
        $location = Location::find($id); 
        
        $imgCount = Image::where('location_id','LIKE', '%'.$location->id.'%')->count();
        
        if($imgCount > 0){
            return response()->json(['message' => 'Le lieu est associé a des photos, il ne peut être supprimé!'], 200);
        }
        
        elseif($imgCount === 0){
            if ($location->delete()){            
                return response()->json(['id' => $location->id, 'message' => 'Lieux supprimé'.$imgCount],200);
            } else {
                return response()->json(['message' => 'Lieux non trouvé!'], 404);
            }
        }

    }

    public function destroyLoc(Location $location, Request $request, $id)
    {
        $location = Location::find($id); 
        
        $imgCount = Image::where('location_id','LIKE', '%'.$location->id.'%')->count(); 
      

        if($imgCount>0){
            return back()->with('wrong', __("Le lieu est associé a des photos, il ne peut être supprimé!"));
        }
        elseif($imgCount === 0){
            $location->delete();
            return redirect('locations/index')->with('good', __("Le lieu a bien été supprimé!"));
        }        
        
    }
   
        
}
