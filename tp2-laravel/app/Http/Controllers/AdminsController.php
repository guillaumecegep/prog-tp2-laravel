<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Image;
use App\Location;

class AdminsController extends Controller
{
    public function __construct()
    {
 
        $this->middleware('ajax')->only('destroy','destroyAllReports');
        $this->middleware('admin');
        
    }
    public function index()
    {
        $users = User::all();
        return view('admin.index', compact('users'));
    }

    public function reportedList()
    {
        $images = Image::all();
        $images = $this->getReportedImage($images)->where('approved', 1);
        return view('admin.reported_img', compact('images'));
    }

    public function destroyAllReports(Image $image)
    {
        $images = Image::all();
        $images = $this->getReportedImage($images)->where('approved', 1);

        if (!empty($images)){
            foreach($images as $image){
                $image->delete();
            }
            return response()->json(['message' => 'Les images ont été supprimées'],200);
        } else {
            return response()->json(['message' => 'Échec, aucune image supprimée!'], 404);
        }
    }

    public function getReportedImage($images)
    {
        $images->transform(function($image) {
            $number = $image->users->where('pivot.report', 1)->count();
            $image->approved = ($number < 1) ? 0 : 1;
            return $image;
        });
        return $images;
    }
}
