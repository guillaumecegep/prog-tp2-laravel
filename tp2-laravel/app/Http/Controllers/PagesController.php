<?php

namespace App\Http\Controllers;

use App\Location;
use App\Image;
use App\User;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;

class PagesController extends Controller
{

    public function index()
    {
        $images = Image::inRandomOrder()->paginate(100);                         

        $images = $this->getReportedImage($images)->where('approved', 1);                       
      
        return view('welcome', compact('images'));
    }

    

    public function getReportedImage($images)
    {
        $images->transform(function($image) {
            $number = $image->users->where('pivot.report', 1)->count();
            $image->approved = ($number >= 2) ? 0 : 1;
            return $image;
        });
        return $images;

    }
}
