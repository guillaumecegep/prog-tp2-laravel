<?php

namespace App;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $fillable = ['title', 'name', 'location_id','user_id'];

    
    public function location()
    {
        return $this->belongsTo(Location::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function users()
    {
        return $this->belongsToMany(User::class)->withPivot('report');
    }

    public function getUpdatedAtAttribute($date)
    {
        return Carbon::parse($date)->locale('fr')->diffForHumans(Carbon::now());
    }
}
